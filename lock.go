package main
import (
  "time"
  "github.com/patrickmn/go-cache"
)

const (
    expire = 20*time.Minute
    purge = expire
)

type Lock struct {
  c *cache.Cache
}

func NewLock () *Lock {
    return &Lock{ cache.New(expire, purge) }
}

func (l Lock) Try (key string) bool {
    _, found := l.c.Get(key)
    if found {
        return false
    }

    l.c.Set(key, "_", cache.DefaultExpiration)
    return true
}
