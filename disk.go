package main
import (
  "syscall"
)

const (
	B  = 1
	KB = 1024 * B
	MB = 1024 * KB
	GB = 1024 * MB
)

func rootDiskUsage() (float32, float32) {
  fs := syscall.Statfs_t{}
  err := syscall.Statfs("/", &fs)
  if err != nil {
    return 0.0, 100.0
  }

  all := float32(fs.Blocks * uint64(fs.Bsize))
  free := float32(fs.Bavail * uint64(fs.Bsize))
  used := all - free
  return free/float32(GB), used/all*100.0
}
