package main

import (
  "fmt"
  "time"
  "os"
)

const (
    warnThreshold = 97.0
    pollInterval = 20 * time.Second
)

func main() {

  slackKey := os.Getenv("SLACK_KEY")
  slackChannel := os.Getenv("SLACK_CHANNEL")

  ok := true
  if slackKey == "" {
    fmt.Println("Please set your Slack API key via the environment variable: `SLACK_KEY`")
    ok = false
  }

  if slackChannel == ""  {
    fmt.Println("Please set your Slack channel via the environment variable: `SLACK_CHANNEL`")
    ok = false
  }

  if !ok {
    os.Exit(1)
  }

  slack := getSlack(slackKey, slackChannel)
  lock := NewLock()

  hostname, err := os.Hostname()
  if err != nil {
    panic(err)
  }

  for {
    free, usedPercentage := rootDiskUsage()
    if usedPercentage > warnThreshold && lock.Try("disk") {
        report := fmt.Sprintf("Rootfs (free, used%%) = (%.2f GiB, %.2f%%)\n", free, usedPercentage)
        msg := fmt.Sprintf("_RESCUE!!_ I am running out of space!!\n    %s\n", report)

        err := slack.notify(hostname, msg)
        if err != nil {
            fmt.Printf("Reporting: %s", report)
        } else {
            fmt.Printf("Failed to post Slack notification: %s", err)
        }
    }

    time.Sleep(pollInterval)
  }
}
