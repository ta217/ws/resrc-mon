package main

import (
  "github.com/bluele/slack"
)

type mySlack struct {
  api *slack.Slack
  emoji string
  channelName string
}

const (
  channelNameDefault = "general"
  emojiDefault = ":scream:"
)

func (sl mySlack) notify(username string, msg string) error {
  opt := &slack.ChatPostMessageOpt{
    AsUser:    false,
    Username:  username,
    IconEmoji: sl.emoji,
  }

  return sl.api.ChatPostMessage(sl.channelName, msg, opt)
}

func getSlack(key string, channel string) *mySlack {
  return &mySlack{
    api: slack.New(key),
    emoji: ":scream:",
    channelName: channel,
  }
}

