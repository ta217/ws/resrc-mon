
TARGET=resrc-mon

.PHONY: clean

$(TARGET):
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o $@ -a -ldflags '-s -w -extldflags "-static"' .

clean:
	rm -f $(TARGET)
